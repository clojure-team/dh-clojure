
(defproject org.debian/dh-clojure-lein "debian"
  :description "Debian support for packaging Leiningen based Clojure projects"
  :url "https://salsa.debian.org/clojure-team/dh-clojure"
  :licenses [{:name "EPL-2.0" :url "https://www.eclipse.org/legal/epl-2.0/"}
             {:name "LGPL-2.1+" :url "https://www.gnu.org/licenses/lgpl-2.1"}]
  :eval-in-leiningen true
  :exclusions [nrepl/nrepl])
